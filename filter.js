function filter(elements, callBackFunction) {
    let array = [];
    if (elements && callBackFunction && Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            if (callBackFunction(elements[i]) == true) {
                array.push(elements[i]);
            }
        }
    }
    return array;
}

module.exports = filter;