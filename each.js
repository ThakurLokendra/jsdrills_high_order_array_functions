function each(elements, callBackFunction) {
    if (elements && callBackFunction && Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            callBackFunction(elements[i], i);
        }
    }
}

module.exports = each;

