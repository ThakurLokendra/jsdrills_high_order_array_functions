const map = require('/home/lokendra/js2/map.js');
const items = [1, 2, 3, 4, 5, 5];

function iteratee(element,i,elementArr) {
 
    if(element && i && elementArr){
        return element*i*elementArr[1] ;
    }else if(element && i){
        return element *i;
    }else if(element){
        return element*2;
    }else{
        return 'undefined';
    }
    
}


const result = map(items, iteratee);
if(result.length==0){
     console.log('Undefined');
}else{
    console.log(result);
}


