function find(elements, callBackFunction) {
    if (elements && callBackFunction && Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            if (callBackFunction(elements[i]) == true) {
                return elements[i];
            }
        }
    }
    return undefined;
}

module.exports = find;
