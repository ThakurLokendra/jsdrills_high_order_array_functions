function reduce(elements, cb, startingValue){
    let sum;
    if(elements && cb){
        let startIndex =0;
        if(!startingValue){
            startingValue = elements[0];
            startIndex =1;
        }
        sum = startingValue;
        for(let i=startIndex;i< elements.length;i++){
            sum = cb(sum,elements[i],i,elements);
        }
    }
    return sum;
}

module.exports = reduce;
