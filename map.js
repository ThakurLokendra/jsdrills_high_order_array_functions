function map(elements, callBackFunction) {
     const array = [];
     if (elements && callBackFunction && Array.isArray(elements)) {
          for (let i = 0; i < elements.length; i++) {
               let a = callBackFunction(elements[i],i,elements);
               array.push(a);
          }
     }
     return array;
}

module.exports = map;



